use crate::prelude::*;

pub fn compute<T>(
    token: &Token,
    rand: &mut T,
) -> std::result::Result<u16, Box<dyn std::error::Error>>
where
    T: Rand,
{
    let res = match token {
        Token::Value(MetaValue(n)) => *n,
        Token::Roll(MetaRoll { dice, sided }) => {
            let mut total = 0;
            for i in rand.take(*sided as u8, *dice as u8) {
                total += i as u16;
            }
            total
        }
        Token::Highest(MetaHighest {
            highest,
            sided,
            dice,
        }) => {
            if highest >= dice {
                Err(ComputeError(format!(
                    "highest kept rolls ({highest}) must be inferior to dice count ({dice})"
                )))?;
            }

            let mut rand = rand.take(*sided as u8, *dice as u8);
            rand.sort();
            let mut total = 0;
            for i in rand.iter().rev().take(*highest as usize) {
                total += *i as u16;
            }
            total
        }
        Token::Add(MetaAdd(f, s)) => compute(f, rand)? + compute(s, rand)?,
        Token::Sub(MetaSub(f, s)) => compute(f, rand)? - compute(s, rand)?,
        Token::Mul(MetaMul(f, s)) => compute(f, rand)? * compute(s, rand)?,
        Token::Div(MetaDiv(f, s)) => {
            let dividend = compute(f, rand)?;
            let divisor = compute(s, rand)?;
            if divisor == 0 {
                Err(ComputeError(format!("division by 0: {dividend}/0")))?
            }
            dividend / divisor
        }
    };

    Ok(res)
}
