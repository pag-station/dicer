use crate::prelude::*;

pub struct Parser<LexemIterator>
where
    LexemIterator: Iterator<Item = Lexem>,
{
    lexems: LexemIterator,
    left: Option<Lexem>,
    pub latest: Option<Lexem>,
}

impl<Iterator> Parser<Iterator>
where
    Iterator: std::iter::Iterator<Item = Lexem>,
{
    pub fn new(lexems: Iterator) -> Self {
        Self {
            lexems,
            left: None,
            latest: None,
        }
    }

    pub fn parse(&mut self) -> Result<Token> {
        let res = self.parse_calculus()?;
        if let Some(popped) = self.pop() {
            Err(ParseError::new("nothing", Some(popped)))
        } else {
            Ok(res)
        }
    }

    fn parse_calculus(&mut self) -> Result<Token> {
        let first = self.parse_direct_expression()?;
        self.parse_operation(first)
    }

    fn parse_operation(&mut self, first: Token) -> Result<Token> {
        if let Some(lexem) = self.pop() {
            match lexem.lex_type {
                LexType::ParensClose => {
                    self.push(lexem);
                    Ok(first)
                }
                LexType::Symbol('*') => {
                    let third = self.parse_direct_expression()?;
                    self.parse_operation(Token::Mul(MetaMul(Box::new(first), Box::new(third))))
                }
                LexType::Symbol('/') => {
                    let third = self.parse_direct_expression()?;
                    self.parse_operation(Token::Div(MetaDiv(Box::new(first), Box::new(third))))
                }
                LexType::Symbol('+') => Ok(Token::Add(MetaAdd(
                    Box::new(first),
                    Box::new(self.parse_calculus()?),
                ))),
                LexType::Symbol('-') => Ok(Token::Sub(MetaSub(
                    Box::new(first),
                    Box::new(self.parse_calculus()?),
                ))),
                LexType::Symbol(_) => unreachable!(),
                _ => Err(ParseError::new("an operator", Some(lexem))),
            }
        } else {
            Ok(first)
        }
    }

    fn parse_direct_expression(&mut self) -> Result<Token> {
        let ex = self
            .pop()
            .ok_or_else(|| ParseError::new("a value type", None))?;
        let token = match ex.lex_type {
            LexType::Number(n) => self.parse_value_or_value_string(n)?,
            LexType::ParensOpen => {
                let expr = self.parse_calculus()?;
                self.expect_closing_parens(|| ex)?;
                expr
            }
            LexType::ParensClose => Err(ParseError::new("a value type", Some(ex)))?,
            LexType::Symbol(_) => Err(ParseError::new("a value type", Some(ex)))?,
            LexType::Word(w) => self.parse_formula(w)?,
        };
        Ok(token)
    }

    fn parse_value_or_value_string(&mut self, previous_n: u16) -> Result<Token> {
        let token = Token::Value(MetaValue(previous_n));

        let lexem = match self.pop() {
            None => return Ok(token),
            Some(lexem) => lexem,
        };

        let word = match &lexem.lex_type {
            LexType::Word(w) => w,
            _ => {
                self.push(lexem);
                return Ok(token);
            }
        };

        if *word != "d" {
            Err(ParseError::new("a dice expression", Some(lexem)))?;
        }

        let lexem = match self.pop() {
            None => Err(ParseError::new("a dice expression", None))?,
            Some(lexem) => lexem,
        };

        let sided_die = match &lexem.lex_type {
            LexType::Number(number) => number,
            _ => Err(ParseError::new("a dice expression", Some(lexem.clone())))?,
        };

        Ok(Token::Roll(MetaRoll {
            sided: *sided_die,
            dice: previous_n,
        }))
    }

    fn parse_formula(&mut self, word: String) -> Result<Token> {
        if word.as_str() != "highest" {
            Err(self.bad("highest"))?;
        }

        let highest = self.parse_number()?;

        self.parse_precise_word("of")?;
        let dice = self.parse_number()?;

        self.parse_precise_word("d")?;
        let sided = self.parse_number()?;

        Ok(Token::Highest(MetaHighest {
            highest,
            sided,
            dice,
        }))
    }

    fn parse_word(&mut self, expected: &str) -> Result<String> {
        let str = self
            .pop()
            .ok_or_else(|| self.none(expected))?
            .as_word_str()
            .ok_or_else(|| self.bad(expected))?
            .to_owned();
        Ok(str)
    }
    fn parse_number(&mut self) -> Result<u16> {
        let n = self
            .pop()
            .ok_or_else(|| self.none("a number"))?
            .as_number()
            .ok_or_else(|| self.bad("a number"))?
            .to_owned();
        Ok(n)
    }

    fn parse_precise_word(&mut self, expected: &str) -> Result<()> {
        if self.parse_word(expected)?.as_str() == expected {
            Ok(())
        } else {
            Err(self.bad(expected))
        }
    }

    fn bad(&mut self, err: &str) -> ParseError {
        ParseError::new(err, self.latest.clone())
    }

    fn none(&mut self, err: &str) -> ParseError {
        ParseError::new(err, None)
    }

    fn expect_closing_parens(&mut self, lexem: impl FnOnce() -> Lexem) -> Result<()> {
        let expected_paren = self
            .pop()
            .ok_or_else(|| ParseError::new("a closing parens", None))?;
        match expected_paren.lex_type {
            LexType::ParensClose => Ok(()),
            _ => Err(ParseError::new("a closing parens", Some(lexem()))),
        }
    }

    #[must_use]
    fn pop(&mut self) -> Option<Lexem> {
        let left = self.left.take();
        if left.is_some() {
            return left;
        }

        let next = self.lexems.next();
        if next.is_some() {
            self.latest = next.clone();
        }
        next
    }

    fn push(&mut self, lex: Lexem) -> &mut Self {
        self.left = Some(lex);
        self
    }
}
