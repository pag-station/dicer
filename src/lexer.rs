use crate::prelude::*;

#[derive(Debug, PartialEq)]
pub struct Lexer {
    text: Vec<char>,
    index: usize,
    line: usize,
    column: usize,
    last_nl_met: Option<char>,
}

impl Lexer {
    pub fn new(input: &str) -> Lexer {
        let text = input.chars().collect();
        Lexer {
            text,
            index: 0,
            line: 1,
            column: 1,
            last_nl_met: None,
        }
    }

    fn current_char(&self) -> Option<&char> {
        self.text.get(self.index)
    }

    pub fn lex(&mut self) -> Option<LexType> {
        self.skip_whitespaces();

        match self.current_char()? {
            '0'..='9' => Some(LexType::Number(self.consume_numbers())),
            '(' => {
                self.skip_one();
                Some(LexType::ParensOpen)
            }
            ')' => {
                self.skip_one();
                Some(LexType::ParensClose)
            }
            'a'..='z' => Some(LexType::Word(self.consume_word())),
            '+' | '-' | '*' | '/' => Some(LexType::Symbol(self.consume_char())),
            _ => None,
        }
    }

    fn skip_one(&mut self) {
        self.index += 1;
        self.column += 1;
    }

    fn skip_whitespaces(&mut self) {
        while let Some(char) = self.current_char() {
            let char = *char;
            if char == '\n' || char == '\r' {
                self.skip_one();
                if let Some(other) = self.last_nl_met {
                    if char == other {
                        self.column = 1;
                        self.line += 1;
                    } else {
                        // still the same newline, so we remove the +1 column added by skip_one
                        self.column -= 1;
                        self.last_nl_met = None;
                    }
                } else {
                    self.column = 1;
                    self.line += 1;
                    self.last_nl_met = Some(char);
                }
            } else if char.is_ascii_whitespace() {
                self.skip_one();
            } else {
                break;
            }
        }
    }

    fn consume_numbers(&mut self) -> u16 {
        let first = self.index;
        while let Some(char) = self.current_char() {
            if char.is_numeric() {
                self.skip_one();
            } else {
                break;
            }
        }
        self.text[first..self.index]
            .iter()
            .collect::<String>()
            .parse::<u16>()
            .expect("consume number expects a number in")
    }

    fn consume_char(&mut self) -> char {
        let char = *self
            .current_char()
            .expect("consume number expects a number in");
        self.skip_one();
        char
    }

    fn consume_word(&mut self) -> String {
        let first = self.index;
        while let Some(char) = self.current_char() {
            if char.is_ascii_lowercase() || *char == '_' {
                self.skip_one();
            } else {
                break;
            }
        }
        self.text[first..self.index].iter().collect::<String>()
    }
}

impl Iterator for Lexer {
    type Item = Lexem;

    fn next(&mut self) -> Option<Self::Item> {
        let lex_type = self.lex()?;
        let column = self.column;
        let line = self.line;

        Some(Lexem {
            line,
            column,
            lex_type,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_can_lex_number() {
        let mut lexer = Lexer::new("111");
        assert_eq!(lexer.lex().unwrap(), LexType::Number(111));
    }
    #[test]
    fn test_can_lex_parens() {
        let mut lexer = Lexer::new("()");
        assert_eq!(lexer.lex().unwrap(), LexType::ParensOpen);
        assert_eq!(lexer.lex().unwrap(), LexType::ParensClose);
    }

    #[test]
    fn test_can_lex_additions() {
        let mut lexer = Lexer::new("+-");
        assert_eq!(lexer.lex().unwrap(), LexType::Symbol('+'));
        assert_eq!(lexer.lex().unwrap(), LexType::Symbol('-'));
    }

    #[test]
    fn test_can_lex_multiplications() {
        let mut lexer = Lexer::new("*/");
        assert_eq!(lexer.lex().unwrap(), LexType::Symbol('*'));
        assert_eq!(lexer.lex().unwrap(), LexType::Symbol('/'));
    }
    #[test]
    fn test_can_lex_word() {
        let mut lexer = Lexer::new("hello");
        assert_eq!(lexer.lex().unwrap(), LexType::Word(String::from("hello")));
    }

    #[test]
    fn test_can_lex_expression() {
        let mut lexer = Lexer::new("1 + 2 + 3 / 5 * ceil( 19 / 2)");
        assert_eq!(lexer.lex().unwrap(), LexType::Number(1));
        assert_eq!(lexer.lex().unwrap(), LexType::Symbol('+'));
        assert_eq!(lexer.lex().unwrap(), LexType::Number(2));
        assert_eq!(lexer.lex().unwrap(), LexType::Symbol('+'));
        assert_eq!(lexer.lex().unwrap(), LexType::Number(3));
        assert_eq!(lexer.lex().unwrap(), LexType::Symbol('/'));
        assert_eq!(lexer.lex().unwrap(), LexType::Number(5));
        assert_eq!(lexer.lex().unwrap(), LexType::Symbol('*'));
        assert_eq!(lexer.lex().unwrap(), LexType::Word(String::from("ceil")));
        assert_eq!(lexer.lex().unwrap(), LexType::ParensOpen);
        assert_eq!(lexer.lex().unwrap(), LexType::Number(19));
        assert_eq!(lexer.lex().unwrap(), LexType::Symbol('/'));
        assert_eq!(lexer.lex().unwrap(), LexType::Number(2));
        assert_eq!(lexer.lex().unwrap(), LexType::ParensClose);
        assert!(lexer.lex().is_none());
    }
    #[test]
    fn test_can_lex_dice() {
        let mut lexer = Lexer::new("best 3 of 4d6");
        assert_eq!(lexer.lex(), Some(LexType::Word(String::from("best"))));
        assert_eq!(lexer.lex(), Some(LexType::Number(3)));
        assert_eq!(lexer.lex(), Some(LexType::Word(String::from("of"))));
        assert_eq!(lexer.lex(), Some(LexType::Number(4)));
        assert_eq!(lexer.lex(), Some(LexType::Word(String::from("d"))));
        assert_eq!(lexer.lex(), Some(LexType::Number(6)));
        assert!(lexer.lex().is_none());
    }

    #[test]
    fn test_can_iterate() {
        let tokens = Lexer::new("\n\rof 4").collect::<Vec<_>>();

        assert_eq!(
            tokens,
            vec![
                Lexem::new(LexType::Word(String::from("of")), 2, 3),
                Lexem::new(LexType::Number(4), 2, 5),
            ]
        )
    }

    #[test]
    fn test_can_numerate() {
        let token = Lexer::new(" 11").next().expect("should lex");

        assert_eq!(token, Lexem::new(LexType::Number(11), 1, 4))
    }
}
