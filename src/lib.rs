pub mod data;
pub mod lexer;
pub mod parse_error;
pub mod parser;
pub mod program;
pub mod rand;
pub mod straight_ast_calc;
pub mod prelude {
    pub use crate::data::*;
    pub use crate::lexer::Lexer;
    pub use crate::parse_error::*;
    pub use crate::parser::Parser;
    #[allow(unused_imports)]
    pub use crate::program;
    pub use crate::rand::{Rand, URandomRand};

    #[derive(Debug)]
    pub struct ComputeError(pub String);

    impl std::error::Error for ComputeError {}

    impl std::fmt::Display for ComputeError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{}", self.0)
        }
    }
}
