pub trait Rand {
    fn rand_range(&mut self, sided: u8) -> u8;
    fn take(&mut self, sided: u8, dice: u8) -> Vec<u8>;
}

pub struct URandomRand {
    pool: Vec<u8>,
}

impl URandomRand {
    fn rands() -> Vec<u8> {
        use std::io::Read;
        let mut f = std::fs::File::open("/dev/urandom").unwrap();
        let mut array = [0u8; 20];
        f.read_exact(&mut array).unwrap();
        array.to_vec()
    }

    fn pop(&mut self) -> u8 {
        match self.pool.pop() {
            None => {
                self.pool = Self::rands();
                self.pop()
            }
            Some(v) => v,
        }
    }
}

impl Rand for URandomRand {
    fn rand_range(&mut self, sided: u8) -> u8 {
        1 + (self.pop() % sided)
    }

    fn take(&mut self, sided: u8, dice: u8) -> Vec<u8> {
        (0..dice).map(|_| self.rand_range(sided)).collect()
    }
}

impl Default for URandomRand {
    fn default() -> URandomRand {
        URandomRand {
            pool: Self::rands(),
        }
    }
}
