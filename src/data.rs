use std::fmt::Display;

#[derive(Debug, PartialOrd, PartialEq, Clone)]
pub struct Lexem {
    pub lex_type: LexType,
    pub line: usize,
    pub column: usize,
}

impl Lexem {
    pub fn new(lex_type: LexType, line: usize, column: usize) -> Self {
        Self {
            lex_type,
            line,
            column,
        }
    }

    pub fn as_word_str(&self) -> Option<&str> {
        match &self.lex_type {
            LexType::Word(word) => Some(word.as_str()),
            _ => None,
        }
    }

    pub fn as_number(&self) -> Option<u16> {
        match &self.lex_type {
            LexType::Number(n) => Some(*n),
            _ => None,
        }
    }
}

#[derive(Debug, PartialOrd, PartialEq, Clone)]
pub enum LexType {
    Number(u16),
    ParensOpen,
    ParensClose,
    Symbol(char),
    Word(String),
}

impl Display for LexType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str = match self {
            LexType::Number(n) => n.to_string(),
            LexType::ParensOpen => "(".to_string(),
            LexType::ParensClose => ")".to_string(),
            LexType::Symbol(s) => s.to_string(),
            LexType::Word(w) => w.to_owned(),
        };
        write!(f, "{}", str)
    }
}

#[derive(Debug, PartialOrd, PartialEq)]
pub struct MetaValue(pub u16);

#[derive(Debug, PartialOrd, PartialEq)]
pub struct MetaHighest {
    pub highest: u16,
    pub sided: u16,
    pub dice: u16,
}

#[derive(Debug, PartialOrd, PartialEq)]
pub struct MetaRoll {
    pub sided: u16,
    pub dice: u16,
}

#[derive(Debug, PartialOrd, PartialEq)]
pub struct MetaAdd(pub Box<Token>, pub Box<Token>);
#[derive(Debug, PartialOrd, PartialEq)]
pub struct MetaSub(pub Box<Token>, pub Box<Token>);
#[derive(Debug, PartialOrd, PartialEq)]
pub struct MetaMul(pub Box<Token>, pub Box<Token>);
#[derive(Debug, PartialOrd, PartialEq)]
pub struct MetaDiv(pub Box<Token>, pub Box<Token>);

#[derive(Debug, PartialOrd, PartialEq)]
pub enum Token {
    Value(MetaValue),
    Highest(MetaHighest),
    Roll(MetaRoll),
    Add(MetaAdd),
    Sub(MetaSub),
    Mul(MetaMul),
    Div(MetaDiv),
}
