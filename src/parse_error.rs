use crate::data::Lexem;

#[derive(Debug)]
pub struct ParseError {
    pub expected: String,
    pub unexpected: Option<Lexem>,
}

impl ParseError {
    pub fn new(expected: &str, unexpected: Option<Lexem>) -> Self {
        Self {
            expected: expected.to_string(),
            unexpected,
        }
    }
}

pub type Result<T> = std::result::Result<T, ParseError>;

impl std::error::Error for ParseError {}

impl std::fmt::Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "",)
    }
}
