use dice_stat_checker::prelude::*;
use dice_stat_checker::straight_ast_calc;
use std::env::args;

fn main() {
    let input = args()
        .skip(1)
        .collect::<Vec<String>>()
        .join(" ")
        .to_lowercase();
    let mut parser = Parser::new(Lexer::new(&input));
    let ast = parser.parse();

    if let Err(err) = ast {
        print_error(input, err, parser.latest.take());
        std::process::exit(1);
    }

    let token = ast.expect("already checked");

    let mut rand = URandomRand::default();

    match straight_ast_calc::compute(&token, &mut rand) {
        Err(error) => eprintln!("{error}"),
        Ok(result) => println!("{result}"),
    }
}

fn print_error(input: String, err: ParseError, latest_lexem: Option<Lexem>) {
    eprintln!("Syntax error:");
    let reference = if let Some(mut thing) = err.unexpected {
        let str = thing.lex_type.to_string();
        thing.column -= str.len();
        eprintln!("Expected {} got {str}", err.expected);
        thing
    } else if let Some(latest) = latest_lexem {
        let str = latest.lex_type.to_string();
        eprintln!("Expected {} got nothing after {str}", err.expected);
        latest
    } else {
        eprintln!("No input given");
        std::process::exit(1);
    };
    eprintln!("line | code");
    eprintln!("─────┼─────");
    for (index, line) in input.lines().enumerate() {
        eprintln!("{: >3}  | {line}", index + 1);
        if index + 1 == reference.line {
            eprintln!("     | {}^", " ".repeat(reference.column - 1))
        }
    }
}
